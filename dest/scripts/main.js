'use strict';

(function () {
  if ($('.js-customScroll').length > 0) {
    var myScroll = function myScroll(delta) {
      var $inner = myCS.$inner;
      $inner.animate({ 'scrollTop': $inner.scrollTop() + delta + 'px' }, 100);
    };

    $('.js-customScroll').find('.custom-scroll-advanced_track-y').show();
    var myCS = $('.js-customScroll').customScroll({
      prefix: 'custom-scroll-advanced_',
      offsetTop: 10,
      offsetBottom: 10,
      trackWidth: 8
    });

    var $track = myCS.$container.find('.custom-scroll-advanced_track-y');

    $track.on('click', function (e) {
      var yPos = e.pageY - $(this).offset().top;
      var barTop = myCS.$bar.position().top;
      var h = myCS.$container.height() - 20;
      myScroll(yPos < barTop ? -h : h);
    }).on('click', '.arrow', function (e) {
      e.stopPropagation();
      var isTop = $(this).hasClass('top');
      myScroll(isTop ? -50 : 50);
    });
  } else {
    $('.js-customScroll').find('.custom-scroll-advanced_track-y').hide();
  }

  $('.fixed-menu').affix({
    offset: {
      top: function top() {
        return this.top = $('.header').outerHeight(true);
      }
    }
  });

  $('.about-carousel-wrapper').owlCarousel({
    margin: 140,
    loop: true,
    autoWidth: true,
    nav: true,
    dots: false,
    stagePadding: 80,
    rewind: false,
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 2,
        margin: 20,
        nav: false
      },
      // breakpoint from 480 up
      480: {
        items: 3,
        margin: 20,
        nav: false
      },
      // breakpoint from 768 up
      768: {
        items: 4,
        margin: 20,
        nav: false
      },
      992: {
        items: 5,
        margin: 20,
        nav: true
      },
      1200: {
        items: 5,
        margin: 40,
        nav: true
      },
      1430: {
        items: 6,
        margin: 40,
        nav: true
      }
    }
  });
  $('.history__slider').owlCarousel({
    items: 1,
    autoplay: true,
    autoplayTimeout: 8000,
    dots: false,
    nav: true,
    loop: true,
    rewind: false,
    smartSpeed: 700,
    slideTransition: 'ease-in-out'
  });

  $('.js-about-slider').owlCarousel({
    items: 1,
    lazyLoad: true,
    autoplay: true,
    dotsContainer: '#customOwlDots',
    loop: true,
    rewind: false,
    autoplayTimeout: 8000,
    slideTransition: 'ease-in-out',
    animateOut: 'fade',
    responsive: {
      // breakpoint from 0 up
      0: {
        dots: true,
        nav: true
      },
      // breakpoint from 480 up
      480: {
        dots: true,
        nav: true
      },
      // breakpoint from 768 up
      768: {
        dots: true,
        nav: true
      },
      992: {
        dots: true,
        nav: false
      },
      1200: {
        dots: true,
        nav: false
      },
      1430: {
        dots: true,
        nav: false
      }
    }
  });

  $('.header-links').find('a').on('click', function (e) {
    e.preventDefault();
    var targ = $($(this).attr('href'));
    var fxMenuH = $('.fixed-menu').outerHeight();
    console.log(fxMenuH);
    $('html, body').animate({
      scrollTop: targ.offset().top - fxMenuH
    }, 2000);
  });
  if ($(window).width() > 1279) {
    $('.type-c').paroller({
      factor: 0.3,
      factorXs: 0.1,
      type: 'foreground'
    });
    // $('#parallax-bg-1').paroller({
    //   factor: 0.2,
    //   factorXs: 0.1
    // });
  }

  $('select').select2({
    minimumResultsForSearch: -1
  });
  $('.js-close18').on('click', function () {
    $(this).closest('.page--plus18').fadeOut().removeClass('fadeIn');
  });

  $(window).on('load', function () {
    $('.page--plus18').addClass('fadeIn');
  });

  $('.js-listMore').find('li').each(function () {
    var listLink = $(this).find('a').attr('href');
    $(this).append('<a href="' + listLink + '", class="list-more">подробнее</a>');
  });

  var drawSVG = $('#draw').drawsvg();
  drawSVG.drawsvg('animate');
})();