'use strict';

ymaps.ready(init);
var myMap;
function init() {
    myMap = new ymaps.Map("map1", {
        center: [46.654746, 36.792973],
        behaviors: ['default', 'scrollZoom'],
        zoom: 17
    });
    myMap.controls.add('zoomControl', { left: 5, top: 5 }).add('typeSelector').add('mapTools', { left: 35, top: 5 }).add('searchControl');

    var myPlacemark0 = new ymaps.Placemark([46.654746, 36.792973], { // Создаем метку с такими координатами и суем в переменную
        balloonContent: '<div class="ballon"><img src="img/hh.jpg" class="ll"/><span>Заголовок метки 1</span><br/><p>Немного инфы о том, о сем. Лорем ипсум чото там.</p><img class="close" onclick="myMap.balloon.close()" src="img/close.png"/></div>' // сдесь содержимое балуна в формате html, все стили в css
    }, {
        iconImageHref: 'dest/images/template/map-marker.png', // картинка иконки
        iconImageSize: [57, 69], // размер иконки
        iconImageOffset: [-32, -64], // позиция иконки
        balloonContentSize: [270, 99], // размер нашего кастомного балуна в пикселях
        balloonLayout: "default#imageWithContent", // указываем что содержимое балуна кастомная херь
        // balloonImageHref: 'img/ballon1.png', // Картинка заднего фона балуна
        balloonImageOffset: [-65, -89], // смещание балуна, надо подогнать под стрелочку
        balloonImageSize: [260, 89], // размер картинки-бэкграунда балуна
        balloonShadow: false,
        balloonAutoPan: false // для фикса кривого выравнивания
    });
    /* тоже самое для других меток */
    /* Добавляем */
    myMap.geoObjects.add(myPlacemark0);

    /* Фикс кривого выравнивания кастомных балунов */
    myMap.geoObjects.events.add(['balloonopen'], function (e) {
        var geoObject = e.get('target');
        myMap.panTo(geoObject.geometry.getCoordinates(), {
            delay: 0
        });
    });
}